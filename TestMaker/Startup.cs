﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TestMaker.DAO.DAL;
using TestMaker.DAO.DataInit;
using Mapster;
using TestMaker.DAO.Service;
using TestMaker.DAO.TestServiceImpl;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.Formatters;
using System.Reflection;
using TestMaker.DAO.TestService;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.SignalR;

namespace TestMaker
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public Startup(IHostingEnvironment env)
        {
            #region mappster config
            TypeAdapterConfig.GlobalSettings.Scan(Assembly.GetEntryAssembly());
            #endregion
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "TestMaker", Version = "v1" });
            });

            //Cross enbale
            #region CrossOrgin
            services.AddCors(options =>
            {
                // TODO: W domyślnym srodowisku nie powinno się zezwalać na taką konfigurację.
                options.AddPolicy("AllowAll",
                    p => p.SetIsOriginAllowed(_ =>
                    {
                        return true;
                    })
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            #region signal r
            services.AddSignalR();
            #endregion

            #endregion
            //end Cross enable

            #region mvc
            services.AddMvc(options =>
            {
                options.OutputFormatters.RemoveType<TextOutputFormatter>();
                options.OutputFormatters.RemoveType<StringOutputFormatter>();
                options.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();

            }).AddJsonOptions(options =>
            {
                options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2).
                AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>()); ;
            #endregion


            #region database connection

            services.AddDbContext<ApplicationDbContext>(options =>
              options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            #endregion

            #region services
            services.AddTransient<IQuizzes,QuizzesServiceImpl>();
            services.AddTransient<IQuestion, QuestionsServiceImpl>();
            services.AddTransient<IAnswer, AnswerServiceImpl>();
            services.AddTransient<IResult, ResultServiceImpl>();
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            #region cors
            app.UseCors("AllowAll");

            app.UseSignalR(_ =>
            {
                _.MapHub<NotifyHub>("/notify");
            });
            #endregion

            #region swagger add
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "TestMakerApi V1");
            });
            #endregion

            #region init database 
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                var dbContext =
                    serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                //stworzenie początkowej bazy, oraz wypełnienie jej danymi
                dbContext.Database.Migrate();
                dbContext.Seed();
            }
            #endregion

            #region mvc
            app.UseMvc();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            #endregion
        }
    }
}
