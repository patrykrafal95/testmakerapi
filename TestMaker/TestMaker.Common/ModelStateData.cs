﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMaker.TestMaker.Common
{
    public class ModelStateData
    {
        /// <summary>
        /// 
        /// </summary>
        public List<ModelStateItem> ModelStateItems { get; set; } = new List<ModelStateItem>();
    }

    public class ModelStateItem
    {
        /// <summary>
        /// 
        /// </summary>
        public string Key { get; set; }

        public List<string> Errors { get; set; } = new List<string>();
    }

}
