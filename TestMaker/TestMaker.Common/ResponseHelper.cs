﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Threading.Tasks;

namespace TestMaker.TestMaker.Common
{
    /// <summary>
    /// 
    /// </summary>
    public static class ResponseHelper
    {
        public static ModelStateData ToModelStateData(this ModelStateDictionary modelState)
        {
            ModelStateData data = new ModelStateData();
            foreach (var item in modelState)
            {
                List<string> errors = new List<string>();
                foreach (var error in item.Value.Errors)
                {
                    if (string.IsNullOrEmpty(error.ErrorMessage))
                    {
                        if(error.Exception == null)
                        {
                            errors.Add("Nieznany błąd");
                        }
                        else
                        {
                            errors.Add(error.Exception.GetType().Name);
                        }
                    }
                    else
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                if (errors.Any())
                {
                    data.ModelStateItems.Add(new ModelStateItem() { Key = item.Key, Errors = errors });
                }
            }
            return data;
        }
    }
}
