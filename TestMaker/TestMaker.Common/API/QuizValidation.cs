﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestMaker.DAO.ViewModel;

namespace TestMaker.TestMaker.Common.API
{
    public class QuizValidation : AbstractValidator<QuizViewModel>
    {
        public QuizValidation()
        {
            RuleFor(_ => _.Title)
                .NotEmpty();
        }
    }
}
