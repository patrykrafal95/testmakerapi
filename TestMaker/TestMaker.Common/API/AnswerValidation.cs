﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestMaker.DAO.ViewModel;

namespace TestMaker.TestMaker.Common.API
{
    public class AnswerValidation : AbstractValidator<AnswerViewModel>
    {
        private const int minValue = -5;
        private const int maxValue = 5;
        public AnswerValidation()
        {
            //TODO Answer Walidation

            RuleFor(_ => _.Text)
                .NotEmpty();
            RuleFor(_ => _.Value)
                .InclusiveBetween(minValue, maxValue);
        }
    }
}
