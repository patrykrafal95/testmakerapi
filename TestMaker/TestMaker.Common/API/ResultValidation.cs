﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.ViewModel;

namespace TestMaker.TestMaker.Common.API
{
    public class ResultValidation : AbstractValidator<ResultViewModel>
    {
        public ResultValidation()
        {
            // TODO ResultValidation
            RuleFor(_ => _.Text)
               .NotEmpty();
        }
    }
}
