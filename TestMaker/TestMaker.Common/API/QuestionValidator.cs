﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestMaker.DAO.ViewModel;

namespace TestMaker.TestMaker.Common.API
{
    public class QuestionValidator : AbstractValidator<QuestionViewModel>
    {
        public QuestionValidator()
        {
            // TODO QuestionValidation
            RuleFor(_ => _.Text)
                .NotEmpty();
        }
    }
}
