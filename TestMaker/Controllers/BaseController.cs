﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using NLog;

namespace TestMaker
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class BaseController : ControllerBase
    {
        private readonly Logger _logger = null;

        public BaseController()
        {
            this._logger = LogManager.GetLogger(this.GetType().Name);
            this._logger.Info(this);
        }

    }
}
