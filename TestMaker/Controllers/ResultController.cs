﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TestMaker.DAO.TestService;
using TestMaker.DAO.ViewModel;
using TestMaker.TestMaker.Common;

namespace TestMaker.Controllers
{
    public class ResultController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IHubContext<NotifyHub, INotyfication> _hubContext;

        /// <summary>
        /// 
        /// </summary>
        private readonly IResult _result;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        public ResultController(IResult result, IHubContext<NotifyHub, INotyfication> hubContext)
        {
            _result = result;
            _hubContext = hubContext;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(200,Type = typeof(IEnumerable<ResultViewModel>))]
        [HttpGet,Route("AllResult/{quizId:int}")]
        public async Task<IActionResult> AllResultAsync(int quizId)
        {
            return Ok(await _result.GetAllResultsAsyc(quizId));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(ResultViewModel))]
        [ProducesResponseType(404)]
        [HttpGet,Route("GetResult/{id:int}")]
        public async Task<IActionResult> GetResultAsync(int id)
        {
            var result = await _result.GetResultByIdAsync(id);
            
            if(result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resultViewModel"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(ResultViewModel))]
        [ProducesResponseType(400, Type = typeof(ModelStateData))]
        [ProducesResponseType(500)]
        [HttpPost, Route("AddResult")]
        public async Task<IActionResult> AddResultAsync([FromBody]ResultViewModel resultViewModel)
        {
            await this._hubContext.Clients.All.SendNotifyToAll("Dodano nowy wynik testu");
            return Ok(await this._result.AddResultAsync(resultViewModel));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resultViewModel"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(ResultViewModel))]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPost, Route("UpdateResult")]
        public async Task<IActionResult> UpdateResultAsync([FromBody] ResultViewModel resultViewModel)
        {
            await this._hubContext.Clients.All.SendNotifyToAll("Zaktualizowano wynik testu");
            var result = await _result.FindResultByIdAsync(resultViewModel.ID);
            if(result == null)
            {
                return NotFound();
            }

            return Ok(await _result.UpdateResultAsync(result, resultViewModel));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(ResultViewModel))]
        [ProducesResponseType(404)]
        [HttpGet,Route("RemoveResult/{id:int}")]
        public async Task<IActionResult> RemoveResultAsync(int id)
        {
            var reuslt = await _result.FindResultByIdAsync(id);

            if(reuslt == null)
            {
                return NotFound();
            }

            return Ok(await _result.RemoveResultAsync(reuslt));
        }

    }
}