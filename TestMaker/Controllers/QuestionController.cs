﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TestMaker.DAO.TestService;
using TestMaker.DAO.ViewModel;
using TestMaker.TestMaker.Common;

namespace TestMaker.Controllers
{
    public class QuestionController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IHubContext<NotifyHub, INotyfication> _hubContext;

        /// <summary>
        /// 
        /// </summary>
        private readonly IQuestion _question;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="question"></param>
        public QuestionController(IQuestion question, IHubContext<NotifyHub, INotyfication> hubContext)
        {
            _question = question;
            _hubContext = hubContext;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(200,Type = typeof(IEnumerable<QuestionViewModel>))]
        [HttpGet,Route("AllQuestion")]
        public async Task<IActionResult> AllQuestionAsync()
        {
            return Ok(await this._question.AllQuestionAsync());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(QuestionViewModel))]
        [ProducesResponseType(404)]
        [HttpGet,Route("GetQuestion/{id:int}")]
        public async Task<IActionResult> GetQuestionAsync(int id)
        {
            var question = await this._question.GetQuestionByIdAsync(id);
            if (question == null)
            {
                return NotFound();
            }
            return Ok(question);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="questionView"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(QuestionViewModel))]
        [ProducesResponseType(400, Type = typeof(ModelStateData))]
        [ProducesResponseType(500)]
        [HttpPost, Route("AddQuestion")]
        public async Task<IActionResult> AddQuestionAsync([FromBody]QuestionViewModel questionView)
        {
            await _hubContext.Clients.All.SendNotifyToAll("Dodano nowe pytanie");
            return Ok(await this._question.AddQuestionAsync(questionView));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="questionViewModel"></param>
        /// <returns></returns>
        [ProducesResponseType(200,Type = typeof(QuestionViewModel))]
        [ProducesResponseType(400, Type = typeof(ModelStateData))]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPost,Route("UpdateQuestion")]
        public async Task<IActionResult> UpdateQuestionAsync([FromBody] QuestionViewModel questionViewModel)
        {
            await _hubContext.Clients.All.SendNotifyToAll("Aktualizacja pytania");
            var question = await this._question.FindQuestionByIdAsync(questionViewModel.ID);
            if(question == null)
            {
                return NotFound(); 
            }
            return Ok(await this._question.UpdateQuestionAsync(question, questionViewModel));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ProducesResponseType(200,Type = typeof(QuestionViewModel))]
        [ProducesResponseType(404)]
        [HttpGet,Route("RemoveQuestion/{id:int}")]
        public async Task<IActionResult> RemoveQuestionAsync(int id)
        {
            var question = await this._question.FindQuestionByIdAsync(id);

            if (question == null)
            {
                return NotFound(); 
            }
            return Ok(await this._question.RemoveQuestionAsync(question));
        }

        [ProducesResponseType(200, Type = typeof(IEnumerable<QuestionViewModel>))]
        [HttpGet,Route("AllQuiz/{id:int}")]
        public async Task<IActionResult> AllQuizByIdAsync(int id)
        {
            var questions = await _question.GetAllQuestionById(id);

            return Ok(questions);
        }

    }
}