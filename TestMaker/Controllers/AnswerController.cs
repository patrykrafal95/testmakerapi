﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TestMaker.DAO.TestService;
using TestMaker.DAO.ViewModel;
using TestMaker.TestMaker.Common;

namespace TestMaker.Controllers
{
    public class AnswerController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IHubContext<NotifyHub, INotyfication> _hubContext;

        /// <summary>
        /// 
        /// </summary>
        private readonly IAnswer _answer;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="answer"></param>
        public AnswerController(IAnswer answer,
            IHubContext<NotifyHub,INotyfication> hubContext)
        {
            _answer = answer;
            _hubContext = hubContext;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(IEnumerable<AnswerViewModel>) )]
        [HttpGet,Route("AllAnswers/{id:int}")]
        public async Task<IActionResult> AllAnswersAsync(int id) => 
            Ok(await this._answer.GetAllAnswerAsync(id));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ProducesResponseType(200,Type = typeof(AnswerViewModel))]
        [ProducesResponseType(404)]
        [HttpGet,Route("GetAnswer/{id:int}")]
        public async Task<IActionResult> GetAnswerAsync(int id)
        {
            var answer = await this._answer.GetAnswerByIdAsync(id);

            if(answer == null)
            {
                return NotFound();
            }
            
            return Ok(answer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="answerViewModel"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(AnswerViewModel))]
        [ProducesResponseType(400, Type = typeof(ModelStateData))]
        [ProducesResponseType(500)]
        [HttpPost, Route("AddAnswer")]
        public async Task<IActionResult> AddAnswerAsync(AnswerViewModel answerViewModel)
        {
            await _hubContext.Clients.All.SendNotifyToAll("Dodano nową odpowiedź");
            return Ok(await this._answer.AddAnswerAsync(answerViewModel));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="answerViewModel"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(AnswerViewModel))]
        [ProducesResponseType(400, Type = typeof(ModelStateData))]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPost, Route("UpdateAnswer")]
        public async Task<IActionResult> UpdateAnswerAsync(AnswerViewModel answerViewModel)
        {
            var answer = await this._answer.FindAnserByIdAsync(answerViewModel.ID);
            if(answer == null)
            {
                return NotFound();
            }
            await _hubContext.Clients.All.SendNotifyToAll("Zaktualizowano odpowiedź");
            return Ok(await this._answer.UpdateAnswerAsync(answer, answerViewModel));
        }

        [ProducesResponseType(200, Type = typeof(AnswerViewModel))]
        [ProducesResponseType(404)]
        [HttpGet, Route("RemoveAnswer/{id:int}")]
        public async Task<IActionResult> RemoveAnswerAsync(int id)
        {
            var answer = await this._answer.FindAnserByIdAsync(id);
            if (answer == null)
            {
                return NotFound();
            }
            return Ok(await this._answer.RemoveAnswerAsync(answer));
        }

    }
}