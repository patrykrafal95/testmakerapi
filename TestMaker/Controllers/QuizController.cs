﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Description;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.Service;
using TestMaker.DAO.TestService;
using TestMaker.DAO.ViewModel;
using TestMaker.TestMaker.Common;

namespace TestMaker.Controllers
{
    public class QuizController : BaseController
    {

        /// <summary>
        /// 
        /// </summary>
        private readonly IHubContext<NotifyHub, INotyfication> _hubContext;

        /// <summary>
        /// 
        /// </summary>
        private readonly IQuizzes _quizzes = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quizzes"></param>
        public QuizController(IQuizzes quizzes, IHubContext<NotifyHub, INotyfication> hubContext)
        {
            _quizzes = quizzes;
            _hubContext = hubContext;
        }
            



        // GET: api/Quiz
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(200,Type = typeof(IEnumerable<QuizViewModel>))]
        [HttpGet,Route("GetAllQuizes")]
        public async Task<IActionResult> GetAllQuizesAsync() => 
            Ok(await this._quizzes.GetAllQuizesAsync());

        // GET: api/Quiz/5
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ProducesResponseType(200,Type = typeof(QuizViewModel))]
        [ProducesResponseType(404)]
        [HttpGet, Route("GetQuiz/{id:int}")]
        public async Task<IActionResult> GetQuizAsync(int id)
        {
            var quiz = await _quizzes.GetQuizByIdAsync(id);
            if(quiz == null)
            {
                return NotFound();
            }

            return Ok(quiz);
        }

        /// <summary>
        /// latest quiz with optional num
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        [ProducesResponseType(200,Type = typeof(IEnumerable<QuizViewModel>))]
        [HttpGet, Route("Latest/{num:int?}")]
        public async Task<IActionResult> LatestQuizAsync(int num = 10) => 
            Ok(await this._quizzes.LatestQuizAsync(num));

        /// <summary>
        /// Get random quiz with optional num
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        [ProducesResponseType(200,Type = typeof(IEnumerable<QuizViewModel>))]
        [HttpGet, Route("Random/{num:int?}")]
        public async Task<IActionResult> RamdomQuizAsync(int num = 10) => 
            Ok(await this._quizzes.RandomQuizGenerateAsync(num));

        /// <summary>
        /// sort quiz by title
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        [ProducesResponseType(200,Type = typeof(IEnumerable<QuizViewModel>))]
        [HttpGet, Route("Oldest/{num:int?}")]
        public async Task<IActionResult> OldestQuizAsync(int num = 10) =>
            Ok(await this._quizzes.OldestQuizAsync(num));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(QuizViewModel))]
        [ProducesResponseType(400, Type = typeof(ModelStateData))]
        [ProducesResponseType(500)]
        [HttpPost, Route("AddQuiz")]
        public async Task<IActionResult> AddQuizAsync([FromBody]QuizViewModel quizView)
        {
            await this._hubContext.Clients.All.SendNotifyToAll("Dodano nowy quiz");
            return Ok(await this._quizzes.AddQuizAsync(quizView));
        }
           

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ProducesResponseType(200,Type = typeof(QuizViewModel))]
        [ProducesResponseType(400, Type = typeof(ModelStateData))]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPost,Route("EditQuiz")]
        public async Task<IActionResult> UpdateQuizAsync([FromBody]QuizViewModel quizViewModel)
        {
            await this._hubContext.Clients.All.SendNotifyToAll("Zaktualizowano quiz");
            var quizFind = await this._quizzes.FindQuizByIdAsync(quizViewModel.ID);
            if(quizFind == null)
            {
                return NotFound();
            }
            return Ok(await this._quizzes.UpdateQuizAsync(quizFind, quizViewModel));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ProducesResponseType(200, Type = typeof(QuizViewModel))]
        [ProducesResponseType(404)]
        [HttpGet, Route("RemoveQuiz/{id:int}")]
        public async Task<IActionResult> RemoveQuizAsync(int id)
        {
            var quizFind = await this._quizzes.FindQuizByIdAsync(id);
            if (quizFind == null)
            {
                return NotFound();
            }
            return Ok(await this._quizzes.RemoveQuizAsync(quizFind));
        }

        [ProducesResponseType(200)]
        [HttpGet,Route("QuizHaveQuesion/{quizId:int}")]
        public async Task<IActionResult> CheckQuizHaveAnyQuestion(int quizId)
        {
            return Ok(await this._quizzes.CheckQuizHaveAnyQuestionAsync(quizId));
        }
    }
}
