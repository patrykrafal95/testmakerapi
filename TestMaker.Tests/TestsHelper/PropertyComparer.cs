﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace TestMaker.Tests.TestsHelper
{
    public class PropertyComparer<T> : IEqualityComparer<T> where T : class
    {

        private readonly PropertyInfo _propertyInfo;

        public PropertyComparer(string propertyInfo)
        {
            this._propertyInfo = typeof(T).GetProperty(propertyInfo,
                BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.Public);
            if(_propertyInfo == null)
            {
                throw new PropertyInfoException($"Property {propertyInfo} not exist {typeof(T)}");
            }
        }

        public bool Equals(T x, T y)
        {
            object xValue = this._propertyInfo.GetValue(x, null);
            object yValue = this._propertyInfo.GetValue(y, null);

            if(xValue == null)
            {
                return yValue == null;
            }
            return xValue.Equals(yValue);
        }

        public int GetHashCode(T obj)
        {
            object propertyValue = this._propertyInfo.GetValue(obj, null);

            if(propertyValue == null)
            {
                return 0;
            }
            else
            {
                return propertyValue.GetHashCode();
            }
        }
    }
}
