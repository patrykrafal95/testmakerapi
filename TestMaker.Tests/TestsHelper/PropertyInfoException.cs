﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestMaker.Tests.TestsHelper
{
    public class PropertyInfoException : Exception
    {
        public PropertyInfoException(string exception) : base(exception)
        {

        }
    }
}
