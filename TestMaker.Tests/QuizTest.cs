﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestMaker.DAO.DAL;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.Service;
using TestMaker.DAO.TestServiceImpl;
using TestMaker.DAO.ViewModel;
using TestMaker.Tests.TestAsyncPrepare;
using TestMaker.Tests.TestsHelper;
using Xunit;

namespace TestMaker.Tests
{
    public class QuizTest
    {
        private readonly Mock<DbSet<Quiz>> repos;
        private readonly Mock<ApplicationDbContext> moqContext;
        private readonly QuizzesServiceImpl moqService;
        private readonly IEqualityComparer<QuizViewModel> comparer;

        public QuizTest()
        {
            //arrange data test
            comparer = new PropertyComparer<QuizViewModel>("ID");
            MockPepare(out repos, out moqContext);
            moqService = MockService(repos, moqContext);
        }

        #region prepare quiz data
        private IQueryable<Quiz> QuizesColections { get; } = InitData(12).AsQueryable();

        private static IEnumerable<Quiz> InitData(int elem)
        {
            DateTime dateTime = DateTime.Now;
            for (int i = 0; i < elem; i++)
            {
                yield return new Quiz()
                {
                    ID = i,
                    User = new ApplicationUser { Email = "patryk@o2.pl" },
                    CreatedDate = dateTime.AddDays(-i),
                    Text = $"Text {i}",
                    Questions = new List<Question>()
                    {
                        new Question()
                        {
                            ID = i
                        }
                    }
                };
            }
        }

        #endregion

        #region test quiz prop
        [Fact]
        public void TestPropertyQuiz()
        {
            Guid userId = Guid.NewGuid();
            DateTime dateTime = DateTime.Now;

            var quiz = new Quiz()
            {
                ID = 1,
                Notes = "Test",
                Type = 1,
                UserId = userId,
                CreatedDate = dateTime,
                ViewCount = 3
            };

            Assert.IsType<Quiz>(quiz);
            Assert.Equal(1, quiz.ID);
            Assert.Null(quiz.Text);
            Assert.Equal(quiz.UserId, userId);
            Assert.Equal(quiz.CreatedDate, dateTime);
        }
        #endregion

        #region DbSetMoq prepare
        private static Mock<DbSet<T>> CreateDbSetMock<T>(IEnumerable<T> param)
        where T : class
        {
            var query = param.AsQueryable();
            var dbSetMoq = new Mock<DbSet<T>>();

            dbSetMoq.As<IAsyncEnumerable<T>>()
            .Setup(m => m.GetEnumerator())
            .Returns(new TestAsyncEnumerator<T>(query.GetEnumerator()));

            dbSetMoq.As<IQueryable<T>>()
            .Setup(m => m.Provider)
            .Returns(new TestAsyncQueryProvider<T>(query.Provider));

            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.Expression)
                .Returns(query.Expression);
            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.ElementType)
                .Returns(query.ElementType);
            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.GetEnumerator())
                .Returns(query.GetEnumerator());

            return dbSetMoq;
        }
        #endregion

        #region test find by id
        [Fact]
        public async Task TestFinByIdQuiz()
        {

            //test
            var prepareQuiz = await moqService.GetQuizByIdAsync(5);

            //assert
            Assert.NotNull(prepareQuiz);
            Assert.IsAssignableFrom<IQuizzes>(moqService);
            Assert.IsAssignableFrom<QuizViewModel>(prepareQuiz);
            Assert.Equal(5, prepareQuiz.ID);
            Assert.Equal("Text 5", prepareQuiz.Text);
            Assert.Null(prepareQuiz.Description);
            moqContext.VerifyGet(_ => _.Quizzes, Times.AtLeastOnce());
            moqContext.Verify();
            repos.Verify();
        }
        #endregion

        #region PrepareObjectQuizess

        private static QuizzesServiceImpl MockService(Mock<DbSet<Quiz>> repos, Mock<ApplicationDbContext> moqContext)
        {
            moqContext.Setup(_ => _.Quizzes).Returns(repos.Object);
            var moqService = new QuizzesServiceImpl(moqContext.Object);
            return moqService;
        }

        private void MockPepare(out Mock<DbSet<Quiz>> repos, out Mock<ApplicationDbContext> moqContext)
        {
            repos = CreateDbSetMock<Quiz>(QuizesColections);
            moqContext = new Mock<ApplicationDbContext>();
        }
        #endregion

        #region test find all
        [Fact]
        public async Task TestAllElementQuiz()
        {

            //test
            var prepareQuiz = await moqService.GetAllQuizesAsync();

            //assert
            Assert.NotNull(prepareQuiz);
            Assert.IsAssignableFrom<IQuizzes>(moqService);
            Assert.IsAssignableFrom<IEnumerable<QuizViewModel>>(prepareQuiz);
            Assert.Equal(12, prepareQuiz.Count());
            moqContext.VerifyGet(_ => _.Quizzes, Times.Once);
            moqContext.Verify();
            repos.Verify();

        }
        #endregion

        #region latest quizzes
        [Theory]
        [InlineData(2)]
        public async Task GetLatestQuiz(int num)
        {

            var prepareQuiz = await moqService.LatestQuizAsync(num);

            Assert.IsAssignableFrom<IQuizzes>(moqService);
            Assert.IsAssignableFrom<IEnumerable<QuizViewModel>>(prepareQuiz);
            Assert.Equal(2, prepareQuiz.Count());
            moqContext.VerifyGet(_ => _.Quizzes, Times.Once);
            moqContext.Verify();
            repos.Verify();
        }
        #endregion

        #region random quizzes
        [Theory]
        [InlineData(8)]
        public async Task RandomQuiz(int num)
        {

            var prepareQuiz = await moqService.RandomQuizGenerateAsync(num);

            Assert.NotNull(prepareQuiz);
            Assert.IsAssignableFrom<IQuizzes>(moqService);
            Assert.IsAssignableFrom<IEnumerable<QuizViewModel>>(prepareQuiz);
            Assert.Equal(num, prepareQuiz.Count());
            moqContext.VerifyGet(_ => _.Quizzes, Times.Once);
            moqContext.Verify();
            repos.Verify();

        }
        #endregion

        #region mappster test
        [Fact]
        public async Task MappsterTestQuiz()
        {

            //test
            var prepareQuiz = await moqService.GetQuizByIdAsync(5);
            var testM = prepareQuiz.Adapt<Quiz>();

            var testList = await moqService.GetAllQuizesAsync();
            var testMList = testList.Adapt<IEnumerable<QuizViewModel>>();
            var isTheSameValue = testList.Except(testMList, comparer);

            Assert.NotNull(testMList);
            Assert.NotNull(testList);
            Assert.True(!isTheSameValue.Any());
            Assert.IsAssignableFrom<IQuizzes>(moqService);
            Assert.IsAssignableFrom<QuizViewModel>(prepareQuiz);
            Assert.IsAssignableFrom<Quiz>(testM);
            Assert.Equal(prepareQuiz.ID, testM.ID);
            Assert.Equal(prepareQuiz.Text, testM.Text);
            Assert.Equal(prepareQuiz.Title, testM.Title);
            Assert.Equal(prepareQuiz.Description, testM.Description);
            moqContext.Verify();
            repos.Verify();
        }
        #endregion

        #region add quiz test
        [Fact]
        public async Task AddQuizNullExceptedException()
        {

            //test
            var prepareQuiz = await moqService.GetQuizByIdAsync(5);
            var quiz = new QuizViewModel
            {
                ID = 3232,
                Text = "dfdfdsfds"
            };

            //var quizAdd = await moqService.AddQuizAsync(quiz);
            await Assert.ThrowsAsync<ArgumentNullException>(() => moqService.AddQuizAsync(quiz));
            //Assert.IsAssignableFrom<IQuizzes>(moqService);
            //Assert.IsAssignableFrom<QuizViewModel>(prepareQuiz);
            //Assert.IsAssignableFrom<QuizViewModel>(quiz);
            //Assert.IsAssignableFrom<QuizViewModel>(quizAdd);
            //Assert.NotNull(quizAdd);
            //Assert.Equal(quizAdd.ID, quiz.ID);
            //Assert.Equal(quizAdd.Text, quiz.Text);;
            //moqContext.Verify(_ => _.SaveChanges(), Times.AtMostOnce());
            //repos.Verify(_ => _.Add(It.IsAny<Quiz>()), Times.Never);
            //moqContext.Verify();
            //repos.Verify();
        }
        #endregion

        #region add quiz test
        [Fact]
        public async Task UpdateQuizTest()
        {

            //test
            var test = await moqService.FindQuizByIdAsync(5);
            var prepareQuiz = new QuizViewModel { ID = 5, Text = "Patryk" };
            var updateAdapterTest = prepareQuiz.Adapt(test);

            Assert.NotNull(updateAdapterTest?.User);
            Assert.IsAssignableFrom<Quiz>(test);
            Assert.IsAssignableFrom<QuizViewModel>(prepareQuiz);
            Assert.IsAssignableFrom<Quiz>(updateAdapterTest);
            Assert.Equal(prepareQuiz.Text, updateAdapterTest.Text);
            Assert.Equal(prepareQuiz.ID, updateAdapterTest.ID);
            moqContext.VerifyGet(_ => _.Quizzes, Times.Once);
            moqContext.Verify();
            repos.Verify();
        }
        #endregion

        #region quiz delete 
        [Fact]
        public async Task RemoveQuizTest()
        {

            //test
            var prepareQuiz = await moqService.FindQuizByIdAsync(5);
            var quizRemoeve = await moqService.RemoveQuizAsync(prepareQuiz);

            Assert.IsAssignableFrom<IQuizzes>(moqService);
            Assert.IsAssignableFrom<Quiz>(prepareQuiz);
            Assert.IsAssignableFrom<QuizViewModel>(quizRemoeve);
            Assert.Equal(quizRemoeve.ID, prepareQuiz.ID);
            Assert.Equal(quizRemoeve.Text, prepareQuiz.Text);
            repos.Verify();
            moqContext.Verify();

        }
        #endregion

        #region get latest quiz
        [Theory]
        [InlineData(5)]
        public async Task LatestQuizAsync(int num)
        {
            var latestDate = await moqService.LatestQuizAsync(num);

            Assert.NotNull(latestDate);
            Assert.Equal(5, latestDate.Count());
            Assert.IsAssignableFrom<IEnumerable<QuizViewModel>>(latestDate);
        }
        #endregion

        #region get oldest quiz
        [Theory]
        [InlineData(5)]
        public async Task OldestQuizAsync(int num)
        {
            var latestDate = await moqService.OldestQuizAsync(num);


            Assert.NotNull(latestDate);
            Assert.Equal(5, latestDate.Count());
            Assert.IsAssignableFrom<IEnumerable<QuizViewModel>>(latestDate);

        }
        #endregion

        #region check quiz have any questin
        [Fact]
        public async Task CheckQuizHaveAnyQuestion()
        {
            var isQuestion = await moqService.CheckQuizHaveAnyQuestionAsync(3);
            var notQuestion = await moqService.CheckQuizHaveAnyQuestionAsync(34234234);

            Assert.True(isQuestion);
            Assert.False(notQuestion);
        }
        #endregion

    }
}
