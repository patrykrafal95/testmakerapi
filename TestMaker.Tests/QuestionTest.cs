﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestMaker.DAO.DAL;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.TestServiceImpl;
using TestMaker.DAO.ViewModel;
using TestMaker.Tests.TestAsyncPrepare;
using TestMaker.Tests.TestsHelper;
using Xunit;

namespace TestMaker.Tests
{
    public class QuestionTest
    {
        private readonly Mock<DbSet<Question>> repos;
        private readonly Mock<ApplicationDbContext> moqContext;
        private readonly QuestionsServiceImpl questionsServiceImpl;
        private readonly IEqualityComparer<QuestionViewModel> comparer;

        #region ctr
        public QuestionTest()
        {
            comparer = new PropertyComparer<QuestionViewModel>("ID");
            MockPepare(out repos, out moqContext);
            questionsServiceImpl = MockService(repos, moqContext);
        }
        #endregion
        private readonly IQueryable<Question> QuestionCollections = PrepareData(10).AsQueryable();
        #region prepare qustions
        private static IEnumerable<Question> PrepareData(int count)
        {
            for (int i = 0; i < count; i++)
            {
                yield return new Question
                {
                    ID = i,
                    CreatedDate = DateTime.Now,
                    QuizId = i,
                    Text = string.Format("Pytanie {0}", i)
                };
            }

        }
        #endregion

        #region indexer
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idx"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        private Question this[int idx, int count]
        {
            get
            {
                foreach (var item in PrepareData(count))
                {
                    if (item.ID == idx)
                    {
                        return item;
                    }
                }
                return null;
            }
        }
        #endregion

        #region DbSetMoq prepare
        private static Mock<DbSet<T>> CreateDbSetMock<T>(IEnumerable<T> param)
        where T : class
        {
            var query = param.AsQueryable();
            var dbSetMoq = new Mock<DbSet<T>>();

            dbSetMoq.As<IAsyncEnumerable<T>>()
            .Setup(m => m.GetEnumerator())
            .Returns(new TestAsyncEnumerator<T>(query.GetEnumerator()));

            dbSetMoq.As<IQueryable<T>>()
            .Setup(m => m.Provider)
            .Returns(new TestAsyncQueryProvider<T>(query.Provider));

            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.Expression)
                .Returns(query.Expression);
            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.ElementType)
                .Returns(query.ElementType);
            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.GetEnumerator())
                .Returns(query.GetEnumerator());

            return dbSetMoq;
        }
        private static QuestionsServiceImpl MockService(Mock<DbSet<Question>> repos, Mock<ApplicationDbContext> moqContext)
        {
            moqContext.Setup(_ => _.Questions).Returns(repos.Object);
            var moqService = new QuestionsServiceImpl(moqContext.Object);
            return moqService;
        }

        private void MockPepare(out Mock<DbSet<Question>> repos, out Mock<ApplicationDbContext> moqContext)
        {
            repos = CreateDbSetMock<Question>(QuestionCollections);
            moqContext = new Mock<ApplicationDbContext>();
        }
        #endregion

        [Fact]
        public void QuestionMappsterCheck()
        {
            var question = new Question
            {
                ID = 1,
                CreatedDate = DateTime.Now,
                QuizId = 1,
                Text = "fdfsd",
                Notes = "fdfsd",
                Quiz = new Quiz { ID = 1, Text = "FDFDS" }
            };
            var mapsterVerify = question.Adapt<QuestionViewModel>();

            Assert.NotNull(mapsterVerify);
            Assert.True(this.PropertyIsValid(question, mapsterVerify));
            Assert.IsAssignableFrom<QuestionViewModel>(mapsterVerify);
        }

        private bool PropertyIsValid(Question question, QuestionViewModel questionViewModel)
        {
            return questionViewModel.ID == question.ID
                && questionViewModel.Text == question.Text
                && questionViewModel.QuizId == question.QuizId;
        }

        #region check test find question by id
        [Fact]
        public async Task GetAllQuestionTasksAsync()
        {


            var questions = await questionsServiceImpl.AllQuestionAsync();
            var checkMappingToQuuestion = this.QuestionCollections.AsEnumerable().Adapt<IEnumerable<QuestionViewModel>>();

            //var isValid = CheckListAreTheSame(questions.ToList(), this.QuestionCollections.ToList());
            var isValid = questions.Except(checkMappingToQuuestion, comparer);

            Assert.True(!isValid.Any());
            Assert.Equal(checkMappingToQuuestion.Count(), questions.Count());
            Assert.IsAssignableFrom<IEnumerable<QuestionViewModel>>(questions);
            Assert.IsAssignableFrom<IEnumerable<QuestionViewModel>>(checkMappingToQuuestion);
            moqContext.Verify();
            moqContext.VerifyGet(_ => _.Questions, Times.Once);
            repos.Verify();
        }

        #region check list before mapper is the same with after mapper
        private bool CheckListAreTheSame(IList<QuestionViewModel> questionsViewModel, IList<Question> checkMappingToQuuestion)
        {
            if (questionsViewModel.Count() != checkMappingToQuuestion.Count()) return false;
            return questionsViewModel.All(_ => checkMappingToQuuestion.Any(
                l => _.ID == l.ID && _.QuizId == l.QuizId && _.Text == l.Text));
        }
        #endregion

        #endregion

        #region test find by id
        [Fact]
        public async Task TestFindQuestionByIdAsynnc()
        {
            var find = await questionsServiceImpl.GetQuestionByIdAsync(2);

            QuestionTest question = new QuestionTest();

            Assert.True(this.PropertyIsValid(question[idx: 2, count: 10], find));
            Assert.False(this.PropertyIsValid(question[idx: 3, count: 10], find));
            Assert.NotNull(find);
            Assert.IsAssignableFrom<QuestionViewModel>(find);
            moqContext.Verify();
            moqContext.VerifyGet(_ => _.Questions, Times.Once);
            repos.Verify();

        }
        #endregion

        #region test delete by id
        [Fact]
        public async Task DeleteQuestionByIdAsync()
        {
            var question = await questionsServiceImpl.FindQuestionByIdAsync(2);
            var remove = await questionsServiceImpl.RemoveQuestionAsync(question);



            Assert.NotNull(remove);
            Assert.IsType<QuestionViewModel>(remove);
            moqContext.Verify();
            repos.Verify();
        }
        #endregion

        #region update question
        [Fact]
        public async Task UpdateQuestion()
        {

            var question = await questionsServiceImpl.FindQuestionByIdAsync(0);
            var questionViewModel = new QuestionViewModel { ID = 1, Text = "GGFDGDF", QuizId = 1 };
            var mappToQuestionViewModel = question.Adapt<QuestionViewModel>();

            Assert.NotNull(mappToQuestionViewModel);
            Assert.IsAssignableFrom<QuestionViewModel>(mappToQuestionViewModel);
            Assert.True(this.PropertyIsValid(question, mappToQuestionViewModel));
            Assert.NotNull(question);
            moqContext.Verify();
            moqContext.VerifyGet(_ => _.Questions, Times.Once);
            repos.Verify();
        }
        #endregion

        #region add question
        [Fact]
        public async Task AddQuestion()
        {

            var question = new QuestionViewModel { ID = 1, QuizId = 1, Text = "ffsdf" };
            var saveResult = await questionsServiceImpl.AddQuestionAsync(question);

            Assert.NotNull(saveResult);
            Assert.IsAssignableFrom<QuestionViewModel>(saveResult);
            moqContext.Verify();
            moqContext.VerifyGet(_ => _.Questions, Times.Once);
            repos.Verify();
        }
        #endregion

        #region check get exception comparator when invalid prop
        [Theory]
        [InlineData("fdsds")]
        public void CheckComparator(string prop)
        {
            Assert.Throws<PropertyInfoException>(() => new PropertyComparer<QuizViewModel>(prop));
           
        }
        #endregion
        #region check get not exception comparator when valid prop
        [Theory]
        [InlineData("ID")]
        [InlineData("Title")]
        [InlineData("Description")]
        [InlineData("Text")]
        public void CheckComparatorIsValidProp(string prop)
        {
            Assert.NotNull(new PropertyComparer<QuizViewModel>(prop));

        }
        #endregion

    }
}
