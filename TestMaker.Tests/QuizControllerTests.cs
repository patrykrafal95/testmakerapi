﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestMaker.Controllers;
using TestMaker.DAO.DAL;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.TestServiceImpl;
using TestMaker.DAO.ViewModel;
using TestMaker.Tests.TestAsyncPrepare;
using Xunit;

namespace TestMaker.Tests
{
    public class QuizControllerTests
    {
        private readonly Mock<DbSet<Quiz>> repos;
        private readonly Mock<ApplicationDbContext> moqContext;
        private readonly QuizzesServiceImpl moqService;
        public QuizControllerTests()
        {
            //arrange data test
            MockPepare(out repos, out moqContext);
            moqService = MockService(repos, moqContext);
        }

        #region prepare quiz data
        private IQueryable<Quiz> QuizesColections { get; } = InitData(12).AsQueryable();

        private static IEnumerable<Quiz> InitData(int elem)
        {
            DateTime dateTime = DateTime.Now;
            for (int i = 0; i < elem; i++)
            {
                yield return new Quiz()
                {
                    ID = i,
                    User = new ApplicationUser { Email = "patryk@o2.pl" },
                    CreatedDate = dateTime.AddDays(-i),
                    Text = $"Text{i}"
                };
            }
        }
        #endregion

        #region DbSetMoq prepare
        private static Mock<DbSet<T>> CreateDbSetMock<T>(IEnumerable<T> param)
        where T : class
        {
            var query = param.AsQueryable();
            var dbSetMoq = new Mock<DbSet<T>>();

            dbSetMoq.As<IAsyncEnumerable<T>>()
            .Setup(m => m.GetEnumerator())
            .Returns(new TestAsyncEnumerator<T>(query.GetEnumerator()));

            dbSetMoq.As<IQueryable<T>>()
            .Setup(m => m.Provider)
            .Returns(new TestAsyncQueryProvider<T>(query.Provider));

            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.Expression)
                .Returns(query.Expression);
            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.ElementType)
                .Returns(query.ElementType);
            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.GetEnumerator())
                .Returns(query.GetEnumerator());

            return dbSetMoq;
        }
        #endregion

        #region PrepareObjectQuizess

        private static QuizzesServiceImpl MockService(Mock<DbSet<Quiz>> repos, Mock<ApplicationDbContext> moqContext)
        {
            moqContext.Setup(_ => _.Quizzes).Returns(repos.Object);
            var moqService = new QuizzesServiceImpl(moqContext.Object);
            return moqService;
        }

        private void MockPepare(out Mock<DbSet<Quiz>> repos, out Mock<ApplicationDbContext> moqContext)
        {
            repos = CreateDbSetMock<Quiz>(QuizesColections);
            moqContext = new Mock<ApplicationDbContext>();
        }
        #endregion

        #region  LatestQuizTest check default value
        [Fact]
        public async Task LatestQuizTest()
        {
            //TODO notyfication tests
            //Arrange
            var controller = new QuizController(moqService, null);

            //Act
            var tests = await controller.LatestQuizAsync();
            var results = Assert.IsAssignableFrom<OkObjectResult>(tests);
            int count = (results.Value as IEnumerable<QuizViewModel>).Count(); 

            //Assert
            Assert.IsAssignableFrom<OkObjectResult>(tests);
            Assert.Equal(10, count);
        }
        #endregion
    }
}
