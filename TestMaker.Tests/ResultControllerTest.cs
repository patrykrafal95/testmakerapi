﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TestMaker.Controllers;
using TestMaker.DAO.DAL;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.TestService;
using TestMaker.DAO.TestServiceImpl;
using TestMaker.DAO.ViewModel;
using TestMaker.TestMaker.Common;
using TestMaker.Tests.TestAsyncPrepare;
using Xunit;

namespace TestMaker.Tests
{
    public class ResultControllerTest
    {
        private readonly Mock<DbSet<Result>> repos;
        private readonly Mock<ApplicationDbContext> moqContext;
        private readonly ResultServiceImpl moqService;
        private readonly Mock<IHubContext<NotifyHub, INotyfication>> _hubContext;

        public ResultControllerTest()
        {
            //arrange data test
            MockPepare(out repos, out moqContext);
            moqService = MockService(repos, moqContext);
            _hubContext = new Mock<IHubContext<NotifyHub, INotyfication>>();
            var clients = new Mock<IHubClients<INotyfication>>();
            _hubContext.Setup(_ => _.Clients).Returns(clients.Object);
            var all = new Mock<INotyfication>();
            _hubContext.Setup(_ => _.Clients.All).Returns(all.Object);
        }

        private IQueryable<Result> ResultColections { get; } = InitData(12).AsQueryable();
        private static IEnumerable<Result> InitData(int elem)
        {
            DateTime dateTime = DateTime.Now;
            for (int i = 0; i < elem; i++)
            {
                yield return new Result()
                {
                    ID = i,
                    CreatedDate = dateTime.AddDays(-i),
                    Text = $"Text{i}",
                    QuizId = i
                };
            }
        }

        #region DbSetMoq prepare
        private static Mock<DbSet<T>> CreateDbSetMock<T>(IEnumerable<T> param)
        where T : class
        {
            var query = param.AsQueryable();
            var dbSetMoq = new Mock<DbSet<T>>();

            dbSetMoq.As<IAsyncEnumerable<T>>()
            .Setup(m => m.GetEnumerator())
            .Returns(new TestAsyncEnumerator<T>(query.GetEnumerator()));

            dbSetMoq.As<IQueryable<T>>()
            .Setup(m => m.Provider)
            .Returns(new TestAsyncQueryProvider<T>(query.Provider));

            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.Expression)
                .Returns(query.Expression);
            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.ElementType)
                .Returns(query.ElementType);
            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.GetEnumerator())
                .Returns(query.GetEnumerator());

            return dbSetMoq;
        }
        #endregion
        #region PrepareObjectQuizess
        private static ResultServiceImpl MockService(Mock<DbSet<Result>> repos, Mock<ApplicationDbContext> moqContext)
        {
            moqContext.Setup(_ => _.Results).Returns(repos.Object);
            var moqService = new ResultServiceImpl(moqContext.Object);
            return moqService;
        }

        private void MockPepare(out Mock<DbSet<Result>> repos, out Mock<ApplicationDbContext> moqContext)
        {
            repos = CreateDbSetMock<Result>(ResultColections);
            moqContext = new Mock<ApplicationDbContext>();
        }
        #endregion

        #region AddResultTests
        [Fact]
        public async Task AddResultTests()
        {
            //TODO notification
            // Arrange & Act
            ResultViewModel resultView = new ResultViewModel();

            var controller = new ResultController(moqService, _hubContext.Object);
            controller.ModelState.AddModelError("Text", "Pole nie może być puste");
            var errors = controller.ModelState.ToModelStateData();

            // Act
            var result = await controller.AddResultAsync(resultView);

            // Assert
            Assert.Single(errors.ModelStateItems);
            Assert.Equal("Pole nie może być puste", errors.ModelStateItems[0].Errors[0]);


        }
        #endregion

        #region Test SignalR Context is not null
        [Fact]
        public void TestSignalRContext()
        {
            Assert.NotNull(_hubContext);
            Assert.NotNull(_hubContext.Object.Clients);
            Assert.NotNull(_hubContext.Object.Clients.All);
        }
        #endregion

        #region Tests SignalR message send
        [Fact]
        public async Task TestsSendMessageToAll()
        {
            await _hubContext.Object.Clients.All.SendNotifyToAll("Witaj");

            _hubContext.VerifyAll();
        }
        #endregion

        #region AddResultValidDataTests
        [Fact]
        public async Task AddResultValidDataTests()
        {
            //Arrange & Act
            ResultViewModel resultView = new ResultViewModel
            {
                ID = 1,
                QuizId = 1,
                Text = "FDS"
            };
            var controller = new ResultController(moqService, _hubContext.Object);
            var hasErrors = controller.ModelState.ToModelStateData();

            //Act
            var result = await controller.AddResultAsync(resultView);

            //Assert
            Assert.IsType<OkObjectResult>(result);
            Assert.Empty(hasErrors.ModelStateItems);
        }
        #endregion

        #region TestIsNotFount
        [Theory]
        [InlineData(65)]
        public  async Task TestIsNotFount(int id)
        {
            //Arrange
            var controller = new ResultController(moqService,null);

            //Act
            var result = await controller.GetResultAsync(id);

            //Assert
            Assert.IsAssignableFrom<NotFoundResult>(result);

        }
        #endregion

        #region TestIsFoundById
        [Theory]
        [InlineData(1)]
        public async Task TestIsFoundById(int id)
        {
            //Arrange
            var controller = new ResultController(moqService,null);

            //Act
            var result = await controller.GetResultAsync(id);

            //Assert
            Assert.IsAssignableFrom<OkObjectResult>(result);

        }
        #endregion

        #region AllResutIsFountById
        [Theory]
        [InlineData(1)]
        public async Task AllResutIsFountById(int id)
        {
            //Arrange
            var controller = new ResultController(moqService,null);

            //Act
            var result = await controller.AllResultAsync(id);
            var returnValue = Assert.IsAssignableFrom<OkObjectResult>(result);
            var resultCount = (returnValue.Value as IEnumerable<ResultViewModel>).Count();

            //Assert
            Assert.Equal(1, resultCount);
        }
        #endregion

        #region TestUpdateAsync
        [Fact]
        public async Task TestUpdateAsync()
        {
            //Arrange
            var model = new ResultViewModel { ID = 32323, Text = "fdfds" };
            var controller = new ResultController(moqService, _hubContext.Object);

            //Act
            var test = await controller.UpdateResultAsync(model);

            //Assert
            Assert.IsAssignableFrom<NotFoundResult>(test);
        }
        #endregion
    }
}
