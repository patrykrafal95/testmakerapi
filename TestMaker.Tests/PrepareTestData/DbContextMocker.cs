﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TestMaker.DAO.DAL;
using TestMaker.DAO.DataInit;

namespace TestMaker.Tests
{
    public class DbContextMocker 
    {
        public static ApplicationDbContext TestMakerDbContext(string dbName)
        {
            //TODO Zmokowanie całej bazy przy użyciu medotdy rozszerzniowej 
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: dbName)
                .Options;

            var dbContext = new ApplicationDbContext(options);

            dbContext.Seed();

            return dbContext;
        }
    }
}
