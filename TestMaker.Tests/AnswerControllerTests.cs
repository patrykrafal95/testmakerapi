﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestMaker.Controllers;
using TestMaker.DAO.DAL;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.TestService;
using TestMaker.DAO.TestServiceImpl;
using TestMaker.DAO.ViewModel;
using TestMaker.TestMaker.Common;
using TestMaker.Tests.TestAsyncPrepare;
using Xunit;

namespace TestMaker.Tests
{
    public class AnswerControllerTests
    {
        
        private readonly Mock<DbSet<Answer>> repos;
        private readonly Mock<ApplicationDbContext> moqContext;
        private readonly AnswerServiceImpl moqService;
        private readonly Mock<IHubContext<NotifyHub, INotyfication>> _hubContext;

        public AnswerControllerTests()
        {
            //arrange data test
            MockPepare(out repos, out moqContext);
            moqService = MockService(repos, moqContext);

            _hubContext = new Mock<IHubContext<NotifyHub, INotyfication>>();
            var clients = new Mock<IHubClients<INotyfication>>();
            _hubContext.Setup(_ => _.Clients).Returns(clients.Object);
            var all = new Mock<INotyfication>();
            _hubContext.Setup(_ => _.Clients.All).Returns(all.Object);
        }

        private IQueryable<Answer> AnswerCollections { get; } = InitData(12).AsQueryable();
        private static IEnumerable<Answer> InitData(int elem)
        {
            DateTime dateTime = DateTime.Now;
            for (int i = 0; i < elem; i++)
            {
                yield return new Answer()
                {
                    ID = i,
                    CreatedDate = dateTime.AddDays(-i),
                    Text = $"Text{i}"
                };
            }
        }

        #region DbSetMoq prepare
        private static Mock<DbSet<T>> CreateDbSetMock<T>(IEnumerable<T> param)
        where T : class
        {
            var query = param.AsQueryable();
            var dbSetMoq = new Mock<DbSet<T>>();

            dbSetMoq.As<IAsyncEnumerable<T>>()
            .Setup(m => m.GetEnumerator())
            .Returns(new TestAsyncEnumerator<T>(query.GetEnumerator()));

            dbSetMoq.As<IQueryable<T>>()
            .Setup(m => m.Provider)
            .Returns(new TestAsyncQueryProvider<T>(query.Provider));

            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.Expression)
                .Returns(query.Expression);
            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.ElementType)
                .Returns(query.ElementType);
            dbSetMoq.As<IQueryable<T>>().Setup(_ => _.GetEnumerator())
                .Returns(query.GetEnumerator());

            return dbSetMoq;
        }
        #endregion
        #region PrepareObjectQuizess
        private static AnswerServiceImpl MockService(Mock<DbSet<Answer>> repos, Mock<ApplicationDbContext> moqContext)
        {
            moqContext.Setup(_ => _.Answers).Returns(repos.Object);
            var moqService = new AnswerServiceImpl(moqContext.Object);
            return moqService;
        }

        private void MockPepare(out Mock<DbSet<Answer>> repos, out Mock<ApplicationDbContext> moqContext)
        {
            repos = CreateDbSetMock<Answer>(AnswerCollections);
            moqContext = new Mock<ApplicationDbContext>();
        }
        #endregion

        #region PrepareObjectQuizess
        private static ResultServiceImpl MockService(Mock<DbSet<Result>> repos, Mock<ApplicationDbContext> moqContext)
        {
            moqContext.Setup(_ => _.Results).Returns(repos.Object);
            var moqService = new ResultServiceImpl(moqContext.Object);
            return moqService;
        }
        #endregion

        #region AddAnswerTestVlidationWasBeFiled
        [Fact]
        public async Task AddAnswerTestVlidationWasBeFiled()
        {
            //Arrange
            var answer = new AnswerViewModel();
            var controller = new AnswerController(moqService, _hubContext.Object);
            const string error = "Pole nie może byc puste";
            controller.ModelState.AddModelError("Text", error);

            //Act
            var action = await controller.AddAnswerAsync(answer);

            //Assert
            Assert.Equal(
                controller.ModelState.ToModelStateData()
                .ModelStateItems[0].Errors[0], error);
        }
        #endregion

        #region AddAnswerTestVlidationWasPass
        [Fact]
        public async Task AddAnswerTestVlidationWasPass()
        {
            //Arrange
            var answer = new AnswerViewModel()
            { ID = 1,  QuestionId = 1, Text = "Przykładowy test", Value = 1};
            var controller = new AnswerController(moqService, _hubContext.Object);

            //Act
            var action = await controller.AddAnswerAsync(answer);

            //Assert
            Assert.IsAssignableFrom<OkObjectResult>(action);
           
        }
        #endregion

        #region GetAnswerByIdOkResult
        [Theory]
        [InlineData(3)]
        public async Task GetAnswerById(int id)
        {
            //Arrange
            var controller = new AnswerController(moqService, _hubContext.Object);

            //Act
            var action = await controller.GetAnswerAsync(id);
            var result = Assert.IsAssignableFrom<OkObjectResult>(action);
            var resultType = (result.Value as AnswerViewModel);
            //Assert
           
            Assert.NotNull(resultType);
            Assert.IsAssignableFrom<AnswerViewModel>(resultType);
        }
        #endregion

        #region GetAnswerByIdNotFount
        [Theory]
        [InlineData(3232)]
        public async Task GetAnswerByIdNotFound(int id)
        {
            //Arrange
            var controller = new AnswerController(moqService, _hubContext.Object);

            //Act
            var action = await controller.GetAnswerAsync(id);

            //Assert
            Assert.IsAssignableFrom<NotFoundResult>(action);
        }
        #endregion



    }
}
