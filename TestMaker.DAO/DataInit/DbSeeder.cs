﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestMaker.DAO.DAL;
using TestMaker.DAO.PublicModel;

namespace TestMaker.DAO.DataInit
{
    public static class DbSeeder
    {
        #region generate simple data
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        public static void Seed(this ApplicationDbContext dbContext)
        {
            if (!dbContext.Users.Any())
            {
                CreateUsers(dbContext);
            }

            if (!dbContext.Questions.Any())
            {
                CreateQuestions(dbContext);
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        private static void CreateQuestions(ApplicationDbContext dbContext)
        {
            DateTime createdDate = new DateTime(2017, 01, 03, 12, 30, 00);
            DateTime lastModiDate = DateTime.Now;

            //pobranie uzytkownika admin
            var adminId = dbContext.Users.
                Where(_ => _.UserName.Equals("Admin"))
                .FirstOrDefault()
                .ID;
            //

#if DEBUG
            //utworzenie 20 przykladowych pytań
            var num = 20;
            for (int i = 0; i < num; i++)
            {
                CreateSampleQuiz(
                    dbContext,
                    i,
                    adminId,
                    3,
                    3,
                    3,
                    3,
                    3,
                    createdDate.AddDays(i));
            }
        }

        private static void CreateSampleQuiz(ApplicationDbContext dbContext, 
            int num, 
            Guid adminId, 
            int viewCount,
            int numOfQues, 
            int numOfAnswerPerQuestion,
            int numOfAnswerQuest, 
            int numOfResults, 
            DateTime created)
        {
            var quiz = new Quiz()
            {
                UserId = adminId,
                Title = String.Format("Tytuł quizu {0}", num),
                Description = String.Format("Przykładowy opis {0}", num),
                Text = "To jest bardzo fajny quiz",
                ViewCount = viewCount,
                CreatedDate = created,
                LastModifiedDate = created
            };

            dbContext.Quizzes.Add(quiz);
            dbContext.SaveChanges();

            for (int i = 0; i < numOfQues; i++)
            {
                var question = new Question()
                {
                    QuizId = quiz.ID,
                    Text = "Pytanie " + i,
                    CreatedDate = created,
                    LastModifiedDate = created
                };
                dbContext.Questions.Add(question);
                dbContext.SaveChanges();

                for (int j = 0; j < numOfAnswerPerQuestion; j++)
                {
                    var answer = dbContext.Answers.Add(new Answer()
                    {
                        QuestionId = question.ID,
                        Text = "Odp testowa. ",
                        Value = j,
                        CreatedDate = created,
                        LastModifiedDate = created
                    });
                }
            }
            for (int i = 0; i < numOfResults; i++)
            {
                dbContext.Results.Add(new Result()
                {
                    QuizId = quiz.ID,
                    Text = "Przykładowy wynik testu.",
                    MinValue = 0,
                    MaxValue = numOfAnswerPerQuestion * 2,
                    CreatedDate = created,
                    LastModifiedDate = created
                });
                dbContext.SaveChanges();
            }
        }

        //

#endif
        #region Create users
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        private static void CreateUsers(ApplicationDbContext dbContext)
        {
            DateTime createdDate = new DateTime(2011, 03, 01, 12, 30, 00);
            DateTime lastModiDate = DateTime.Now;

            var user = new ApplicationUser()
            {
                ID = Guid.NewGuid(),
                UserName = "Admin",
                Email = "admin@o2.pl",
                CreatedDate = createdDate,
                LastModifiedDate = lastModiDate
            };

            dbContext.Users.Add(user);

#if DEBUG
            var user_Kamil = new ApplicationUser()
            {
                ID = Guid.NewGuid(),
                UserName = "Kamil",
                Email = "kamil@o2.pl",
                CreatedDate = createdDate,
                LastModifiedDate = lastModiDate
            };

            var user_Pawel = new ApplicationUser()
            {
                ID = Guid.NewGuid(),
                UserName = "Pawel",
                Email = "pawel@o2.pl",
                CreatedDate = createdDate,
                LastModifiedDate = lastModiDate
            };

            var user_Piotr = new ApplicationUser()
            {
                ID = Guid.NewGuid(),
                UserName = "Piotr",
                Email = "piotr@o2.pl",
                CreatedDate = createdDate,
                LastModifiedDate = lastModiDate
            };

            dbContext.Users.AddRange(user_Kamil, user_Pawel, user_Piotr);

#endif
            dbContext.SaveChanges();

        }
        #endregion
    }
}
