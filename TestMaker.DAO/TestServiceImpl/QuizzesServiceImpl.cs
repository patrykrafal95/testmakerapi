﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestMaker.DAO.DAL;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.Service;
using TestMaker.DAO.ViewModel;

namespace TestMaker.DAO.TestServiceImpl
{
    public class QuizzesServiceImpl : IQuizzes
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ApplicationDbContext _db = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="db"></param>
        public QuizzesServiceImpl(ApplicationDbContext db)
        {
            this._db = db;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public async Task<QuizViewModel> AddQuizAsync(QuizViewModel quizViewModel)
        {
            //mapowanie na quiz from quiz ViewModel
            var quiz = quizViewModel.Adapt<Quiz>();

            //narazie brak autoryzacji dodanie quizu jako admin
            quiz.UserId = await _db.Users.Where(_ => _.UserName == "Admin").Select(_ => _.ID).FirstOrDefaultAsync();
            quiz.CreatedDate = DateTime.Now;
            quiz.LastModifiedDate = quiz.CreatedDate;
            //

            await _db.Quizzes.AddAsync(quiz);
            await _db.SaveChangesAsync();
            return quizViewModel;
        }

        /// <summary>
        /// Get all data about quiz
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<QuizViewModel>> GetAllQuizesAsync()
        {
            return await this._db.Quizzes.ProjectToType<QuizViewModel>().ToListAsync();
        }

        public async Task<Quiz> FindQuizByIdAsync(int id)
        {
            return await _db.Quizzes.Where(_ => _.ID == id).FirstOrDefaultAsync();
        }

        /// <summary>
        /// find quiz by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<QuizViewModel> GetQuizByIdAsync(int id)
        {
            return await _db.Quizzes.Where(_ => _.ID == id).ProjectToType<QuizViewModel>().FirstOrDefaultAsync();
        }

        /// <summary>
        /// get latest element from database
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public async Task<IEnumerable<QuizViewModel>> LatestQuizAsync(int num)
        {
            return await _db.Quizzes
               .OrderByDescending(_ => _.CreatedDate)
               .Take(num).ProjectToType<QuizViewModel>()
               .ToListAsync();
        }

        /// <summary>
        /// get random data using guid 
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public async Task<IEnumerable<QuizViewModel>> RandomQuizGenerateAsync(int num)
        {
            return await _db.Quizzes
                .OrderBy(_ => Guid.NewGuid())
                .Take(num)
                .ProjectToType<QuizViewModel>()
                .ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public async Task<QuizViewModel> RemoveQuizAsync(Quiz quiz)
        {
            _db.Quizzes.Remove(quiz);
            await _db.SaveChangesAsync();
            return quiz.Adapt<QuizViewModel>();
        }

        /// <summary>
        /// Get oldest quizs
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public async Task<IEnumerable<QuizViewModel>> OldestQuizAsync(int num)
        {
            return await _db.Quizzes
                .OrderBy(_ => _.CreatedDate)
                .Take(num)
                .ProjectToType<QuizViewModel>()
                .ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<QuizViewModel> UpdateQuizAsync(Quiz quizModel, QuizViewModel quizViewModel)
        {
            var quiz = quizViewModel.Adapt(quizModel);
            _db.Entry(quiz).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return quizViewModel;
        }

        /// <summary>
        /// check that quiz have any quesiton
        /// </summary>
        /// <param name="quizId"></param>
        /// <returns></returns>
        public async Task<bool> CheckQuizHaveAnyQuestionAsync(int quizId)
        {
            return await this._db.Quizzes
                .Include(_ => _.Questions)
                .AnyAsync(
                _ => _.ID == quizId 
                && _.Questions.Count() > 0);
        }
    }
}
