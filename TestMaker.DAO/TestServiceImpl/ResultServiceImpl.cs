﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestMaker.DAO.DAL;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.TestService;
using TestMaker.DAO.ViewModel;

namespace TestMaker.DAO.TestServiceImpl
{
    public class ResultServiceImpl : IResult
    {
        private readonly ApplicationDbContext _db;

        public ResultServiceImpl(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<ResultViewModel> AddResultAsync(ResultViewModel resultViewModel)
        {
            var result = resultViewModel.Adapt<Result>();
            await this._db.AddAsync(result);
            await this._db.SaveChangesAsync();
            return resultViewModel;
        }

        public async Task<IEnumerable<ResultViewModel>> GetAllResultsAsyc(int quizId)
        {
            return await _db.Results.Where(_ => _.QuizId == quizId).ProjectToType<ResultViewModel>().ToListAsync();
        }

        public async Task<Result> FindResultByIdAsync(int id)
        {
            return await this._db.Results.Where(_ => _.ID == id).FirstOrDefaultAsync();
        }

        public async Task<ResultViewModel> GetResultByIdAsync(int id)
        {
            return await this._db.Results.Where(_ => _.ID == id).ProjectToType<ResultViewModel>().FirstOrDefaultAsync();
        }

        public async Task<ResultViewModel> RemoveResultAsync(Result result)
        {
            this._db.Results.Remove(result);
            await this._db.SaveChangesAsync();
            return result.Adapt<ResultViewModel>();
        }

        public async Task<ResultViewModel> UpdateResultAsync(Result resultModel, ResultViewModel answerViewModel)
        {
            var result = answerViewModel.Adapt(resultModel);
            _db.Entry(result).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return answerViewModel;
        }
    }
}
