﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestMaker.DAO.DAL;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.TestService;
using TestMaker.DAO.ViewModel;

namespace TestMaker.DAO.TestServiceImpl
{
    public class QuestionsServiceImpl : IQuestion
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ApplicationDbContext _db;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="db"></param>
        public QuestionsServiceImpl(ApplicationDbContext db)
        {
            this._db = db;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="questionViewModel"></param>
        /// <returns></returns>
        public async Task<QuestionViewModel> AddQuestionAsync(QuestionViewModel questionViewModel)
        {
            var question = questionViewModel.Adapt<Question>();
            question.CreatedDate = DateTime.Now;
            question.LastModifiedDate = question.CreatedDate;

            await _db.Questions.AddAsync(question);
            await _db.SaveChangesAsync();
            return questionViewModel;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<QuestionViewModel>> AllQuestionAsync()
        {
            return await _db.Questions.ProjectToType<QuestionViewModel>().ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Question> FindQuestionByIdAsync(int id)
        {
            return await _db.Questions.Where(_ => _.ID == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<QuestionViewModel>> GetAllQuestionById(int id)
        {
            return await _db.Questions.Where(_ => _.QuizId == id).ProjectToType<QuestionViewModel>().ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<QuestionViewModel> GetQuestionByIdAsync(int id)
        {
            return await _db.Questions.Where(_ => _.ID == id).ProjectToType<QuestionViewModel>().FirstOrDefaultAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="question"></param>
        /// <returns></returns>
        public async Task<QuestionViewModel> RemoveQuestionAsync(Question question)
        {
            this._db.Questions.Remove(question);
            await this._db.SaveChangesAsync();
            return question.Adapt<QuestionViewModel>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="question"></param>
        /// <param name="questionViewModel"></param>
        /// <returns></returns>
        public async Task<QuestionViewModel> UpdateQuestionAsync(Question question, QuestionViewModel questionViewModel)
        {
            var q = questionViewModel.Adapt(question);
            q.LastModifiedDate = DateTime.Now;
            _db.Entry(q).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return questionViewModel;
        }
    }
}
