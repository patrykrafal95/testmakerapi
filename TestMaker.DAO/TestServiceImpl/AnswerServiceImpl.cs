﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestMaker.DAO.DAL;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.TestService;
using TestMaker.DAO.ViewModel;

namespace TestMaker.DAO.TestServiceImpl
{
    public class AnswerServiceImpl : IAnswer
    {
        private readonly ApplicationDbContext _db;

        public AnswerServiceImpl(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<AnswerViewModel> AddAnswerAsync(AnswerViewModel answerViewModel)
        {
            var answer = answerViewModel.Adapt<Answer>();
            answer.CreatedDate = DateTime.Now;
            answer.LastModifiedDate = answer.CreatedDate;
            await this._db.Answers.AddAsync(answer);
            await this._db.SaveChangesAsync();
            return answerViewModel;
        }

        public async Task<Answer> FindAnserByIdAsync(int id)
        {
            return await _db.Answers.Where(_ => _.ID == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<AnswerViewModel>> GetAllAnswerAsync(int questionId)
        {
            return await _db.Answers.Where(_ => _.QuestionId == questionId).ProjectToType<AnswerViewModel>().ToListAsync();
        }

        public async Task<AnswerViewModel> GetAnswerByIdAsync(int id)
        {
            return await _db.Answers.Where(_ => _.ID == id).ProjectToType<AnswerViewModel>().FirstOrDefaultAsync();
        }

        public async Task<AnswerViewModel> RemoveAnswerAsync(Answer answer)
        {
            _db.Answers.Remove(answer);
            await _db.SaveChangesAsync();
            return answer.Adapt<AnswerViewModel>();
        }

        public async Task<AnswerViewModel> UpdateAnswerAsync(Answer answerModel, AnswerViewModel answerViewModel)
        {
            var answer = answerViewModel.Adapt(answerModel);
            answer.LastModifiedDate = DateTime.Now;
            _db.Answers.Update(answer);
            await _db.SaveChangesAsync();
            return answerViewModel;
        }
    }
}
