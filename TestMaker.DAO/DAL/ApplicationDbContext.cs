﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TestMaker.DAO.PublicModel;

namespace TestMaker.DAO.DAL
{
    public class ApplicationDbContext : DbContext
    {
        #region prop
        public virtual DbSet<ApplicationUser> Users { get; set; }
        public virtual DbSet<Quiz> Quizzes { get; set; }
        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<Answer> Answers { get; set; }
        public virtual DbSet<Result> Results { get; set; }
        #endregion

        #region constr
        public ApplicationDbContext() { }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        #endregion

        #region method configure relations
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>()
                .ToTable("Users");

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(_ => _.Quizzes)
                .WithOne(_ => _.User);

            modelBuilder.Entity<Quiz>()
                .ToTable("Quizess");

            modelBuilder.Entity<Quiz>()
                .Property(_ => _.ID)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Quiz>()
                .HasOne(_ => _.User)
                .WithMany(_ => _.Quizzes);

            modelBuilder.Entity<Quiz>()
                .HasMany(_ => _.Questions)
                .WithOne(_ => _.Quiz);

            modelBuilder.Entity<Question>()
                .ToTable("Questions");

            modelBuilder.Entity<Question>()
                .Property(_ => _.ID)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Question>()
                .HasOne(_ => _.Quiz)
                .WithMany(_ => _.Questions);

            modelBuilder.Entity<Question>()
                .HasMany(_ => _.Answers)
                .WithOne(_ => _.Question);

            modelBuilder.Entity<Answer>()
                .ToTable("Answers");

            modelBuilder.Entity<Answer>()
                .Property(_ => _.ID)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Answer>()
                .HasOne(_ => _.Question)
                .WithMany(_ => _.Answers);

            modelBuilder.Entity<Result>()
                .ToTable("Results");

            modelBuilder.Entity<Result>()
                .Property(_ => _.ID)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Result>()
                .HasOne(_ => _.Quiz)
                .WithMany(_ => _.Results);
        }
        #endregion

    }
}
