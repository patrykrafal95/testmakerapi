﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestMaker.DAO.PublicModel
{
    public class ApplicationUser
    {
        #region constr
        public ApplicationUser() { }
        #endregion

        #region prop
        [Key]
        [Required]
        public Guid ID { get; set; }

        [Required]
        [MaxLength(128)]
        public string UserName { get; set; }

        [Required]
        public string Email { get; set; }

        public string DisplayName { get; set; }
        public string  Notes { get; set; }

        [Required]
        public int  Type { get; set; }

        [Required]
        public int Flags { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        [Required]
        public DateTime LastModifiedDate { get; set; }
        #endregion

        #region relations
        public virtual List<Quiz> Quizzes { get; set; }
        #endregion
    }
}
