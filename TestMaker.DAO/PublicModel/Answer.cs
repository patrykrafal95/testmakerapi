﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TestMaker.DAO.PublicModel
{

    public class Answer
    {

        #region constr
        public Answer() { }

        #endregion

        #region prop
        [Key]
        [Required]
        public int ID { get; set; }

        [Required]
        public int QuestionId { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public int Value { get; set; }

        public string Notes { get; set; }

        [DefaultValue(0)]
        public int Type { get; set; }

        [DefaultValue(0)]
        public int Flags { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        [Required]
        public DateTime LastModifiedDate { get; set; }
        #endregion

        #region relations
        [ForeignKey(nameof(QuestionId))]
        public virtual Question Question{ get; set; }
        #endregion
    }
}
