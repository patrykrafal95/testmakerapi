﻿using Mapster;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TestMaker.DAO.ViewModel;

namespace TestMaker.DAO.PublicModel
{
    public class Quiz
    {
        #region constr
        public Quiz() { }
        #endregion

        #region property
        [Key]
        [Required]
        public int ID { get; set; }

        [Required]
        public string Title { get; set; }

        public string Description { get; set; }

        public string Text { get; set; }

        public string Notes { get; set; }

        [DefaultValue(0)]
        public int Type { get; set; }

        [DefaultValue(0)]
        public int Flags { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        public int ViewCount { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        [Required]
        public DateTime LastModifiedDate { get; set; }

        #endregion

        #region nav prop 
        /// <summary>
        /// Autor quizu: leniwe ładowanie danych z bazy
        /// </summary>
        [ForeignKey(nameof(UserId))]
        public virtual ApplicationUser User { get; set; }

        /// <summary>
        /// Lista pytania do quizu
        /// </summary>
        public virtual List<Question> Questions { get; set; }

        /// <summary>
        /// Lista wyników quizu
        /// </summary>
        public virtual List<Result> Results { get; set; }

        #endregion
    }
}
