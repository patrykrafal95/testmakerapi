﻿using Mapster;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TestMaker.DAO.ViewModel;

namespace TestMaker.DAO.PublicModel
{
    public class Question
    {
        #region constr
        public Question() { }
        #endregion

        #region prop
        [Key]
        [Required]
        public int ID { get; set; }
        [Required]
        public int QuizId { get; set; }

        [Required]
        public string Text { get; set; }

        public string Notes { get; set; }

        [DefaultValue(0)]
        public int Type { get; set; }

        [DefaultValue(0)]
        public int Flags { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        [Required]
        public DateTime LastModifiedDate { get; set; }
        #endregion

        #region relations

        [ForeignKey(nameof(QuizId))]
        public virtual Quiz Quiz { get; set; }

        public virtual List<Answer> Answers { get; set; }

        #endregion

    }

}
