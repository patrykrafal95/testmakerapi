﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestMaker.DAO.ViewModel
{
    public class AnswerViewModel
    {
        #region prop
        public int ID { get; set; }
        public int QuestionId { get; set; }
        public string Text { get; set; }
        public int Value { get; set; }
        #endregion
    }
}
