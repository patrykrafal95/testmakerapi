﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestMaker.DAO.ViewModel
{

    public class QuizViewModel
    {
        #region prop
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
        #endregion
    }
}
