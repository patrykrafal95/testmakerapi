﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestMaker.DAO.ViewModel
{
    public class ResultViewModel
    {
        #region prop
        public int ID { get; set; }
        public int QuizId { get; set; }
        public string Text { get; set; }
        public int? MinValue { get; set; }
        public int? MaxValue { get; set; }
        #endregion
    }
}
