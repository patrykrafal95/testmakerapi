﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace TestMaker.DAO.ViewModel
{
    public class QuestionViewModel
    {
        #region prop
        public int ID { get; set; }
        public int QuizId { get; set; }
        public string Text { get; set; }
        #endregion
    }
}
