﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.ViewModel;

namespace TestMaker.DAO.TestService
{
    public interface IQuestion
    {
        Task<IEnumerable<QuestionViewModel>> AllQuestionAsync();
        Task<QuestionViewModel> GetQuestionByIdAsync(int id);
        Task<Question> FindQuestionByIdAsync(int id);
        Task<QuestionViewModel> AddQuestionAsync(QuestionViewModel questionViewModel);
        Task<QuestionViewModel> UpdateQuestionAsync(Question question, QuestionViewModel questionViewModel);
        Task<QuestionViewModel> RemoveQuestionAsync(Question answer);
        Task<IEnumerable<QuestionViewModel>> GetAllQuestionById(int id);
    }
}
