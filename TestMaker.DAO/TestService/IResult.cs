﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.ViewModel;

namespace TestMaker.DAO.TestService
{
    public interface IResult
    {
        Task<IEnumerable<ResultViewModel>> GetAllResultsAsyc(int quizId);
        Task<ResultViewModel> GetResultByIdAsync(int id);
        Task<Result> FindResultByIdAsync(int id);
        Task<ResultViewModel> AddResultAsync(ResultViewModel resultViewModel);
        Task<ResultViewModel> UpdateResultAsync(Result resultModel, ResultViewModel answerViewModel);
        Task<ResultViewModel> RemoveResultAsync(Result result);
    }
}
