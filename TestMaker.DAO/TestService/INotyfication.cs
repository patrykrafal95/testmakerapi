﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestMaker.DAO.TestService
{
    public interface INotyfication
    {
        Task SendNotifyToAll(string msg);
    }
}
