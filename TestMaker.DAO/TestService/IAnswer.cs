﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.ViewModel;

namespace TestMaker.DAO.TestService
{
    public interface IAnswer
    {
        Task<IEnumerable<AnswerViewModel>> GetAllAnswerAsync(int questionId);
        Task<AnswerViewModel> GetAnswerByIdAsync(int id);
        Task<Answer> FindAnserByIdAsync(int id);
        Task<AnswerViewModel> UpdateAnswerAsync(Answer answerModel, AnswerViewModel answerViewModel);
        Task<AnswerViewModel> AddAnswerAsync(AnswerViewModel answerViewModel);
        Task<AnswerViewModel> RemoveAnswerAsync(Answer answer);
    }
}
