﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestMaker.DAO.PublicModel;
using TestMaker.DAO.ViewModel;

namespace TestMaker.DAO.Service
{
    public interface IQuizzes
    {
        Task<IEnumerable<QuizViewModel>>GetAllQuizesAsync();
        Task<QuizViewModel> GetQuizByIdAsync(int id);
        Task<Quiz> FindQuizByIdAsync(int id);
        Task<IEnumerable<QuizViewModel>> LatestQuizAsync(int num);
        Task<IEnumerable<QuizViewModel>> RandomQuizGenerateAsync(int num);
        Task<IEnumerable<QuizViewModel>> OldestQuizAsync(int num);
        Task<QuizViewModel> AddQuizAsync(QuizViewModel quizViewModel);
        Task<QuizViewModel> UpdateQuizAsync(Quiz findQuiz,QuizViewModel quizFromView);
        Task<QuizViewModel> RemoveQuizAsync(Quiz quiz);
        Task<bool> CheckQuizHaveAnyQuestionAsync(int quizId);
    }
}
